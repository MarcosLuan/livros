A demanda

Dever� ser criada uma aplica��o de cadastro de pessoas:

1) Back-end
A aplica��o, a ser desenvolvida em Java, dever� expor uma API de cadastro, altera��o, remo��o e consulta de pessoas com as seguintes informa��es:
�	Nome - obrigat�rio
�	Sexo
�	E-mail - n�o obrigat�rio, deve ser validado caso preenchido
�	Data de Nascimento - obrigat�rio, deve ser validada
�	Naturalidade
�	Nacionalidade
�	CPF - obrigat�rio, deve ser validado (formato e n�o pode haver dois cadastros com mesmo cpf)
Obs: a data de cadastro e atualiza��o dos dados devem ser armazenados.

2) Front-end
A aplica��o dever� ser acess�vel via navegador e possuir uma tela com formul�rio. N�o h� restri��o em rela��o � tecnologia para o desenvolvimento do frontend.

3) Seguran�a
O acesso � aplica��o s� poder� ser realizado por um usu�rio pr�-existente via autentica��o basic.

4) Instala��o
A aplica��o dever� estar dispon�vel em uma imagem docker a partir do docker-hub e n�o deve exigir configura��es/par�metros. Ou seja, ao rodar a imagem, deve levantar a aplica��o e funcionar.

5) C�digo fonte
A aplica��o dever� possuir um endpoint /source acess�vel sem autentica��o via HTTP GET que dever� retornar o link do projeto no github com o c�digo fonte do projeto desenvolvido.
Importante 1: O projeto dever� ter testes unit�rios da aplica��o.
Importante 2: O projeto dever� utilizar as vers�es mais recentes das tecnologias/frameworks selecionados.
Extras

1.	A aplica��o rodando em algum ambiente em nuvem.
2.	Teste de integra��o da API em linguagem de sua prefer�ncia (Damos import�ncia para pir�mide de testes)
3.	A API desenvolvida em REST, seguindo o modelo de maturidade de Richardson ou utilizando GraphQL.
4.	A API dever� conter documenta��o execut�vel que poder� ser utilizada durante seu desenvolvimento. (Utilizar swagger)
5.	Integra��o com OAuth 2 Google (https://developers.google.com/identity/protocols/OAuth2)
6.	Implementar Chat entre as pessoas que est�o na aplica��o
7.	Vers�o 2 da API que deve incluir endere�o da pessoa como dado obrigat�rio. Vers�o 1 deve continuar funcionando.
Prazo e retorno
Voc� ter� at� o dia 11/06 para realiza��o desse pequeno projeto. Entretanto, realizar a entrega o mais breve poss�vel ser� avaliado positivamente.
