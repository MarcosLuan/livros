<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/cabecalho.jsp"%>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Login Lyncas</title>

<script src="https://apis.google.com/js/platform.js" async defer></script>
<meta name="google-signin-client_id"
	content="603674617065-16quprpuig5l941us84ppmdlq0m267gc.apps.googleusercontent.com">
</head>
<body>
	<div class="g-signin2" data-onsuccess="onSignIn" id="myP"></div>
	<img id="myImg">
	<br>
	<p id="name"></p>
	<div id="status"></div>
	<script type="text/javascript">
			function onSignIn(googleUser) {
				  var profile = googleUser.getBasicProfile();
				  var imagurl=profile.getImageUrl();
				  var name=profile.getName();
				  var email=profile.getEmail();
				  document.getElementById("myImg").src = imagurl;
				  document.getElementById("name").innerHTML = name;
				  document.getElementById("myP").style.visibility = "hidden";
				  document.getElementById("status").innerHTML = 'Bem vindo '+name+'!<a href=books.jsp?email='+email+'/> <br><br>Continue para Google Books!<br><br></a></p>'
			 }
</script>
	<button type="button" onclick="logoutFunction()" class="btn btn-danger btn-sm">Sign Out</button>

	<script>
function logoutFunction() {
	gapi.auth2.getAuthInstance().disconnect();
    location.reload();
}
</script>
</body>
</html>

<%@include file="/rodape.jsp"%>